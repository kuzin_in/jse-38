package ru.kuzin.tm.api.repository;

import ru.kuzin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}