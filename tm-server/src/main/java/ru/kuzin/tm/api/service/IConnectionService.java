package ru.kuzin.tm.api.service;

import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();
}