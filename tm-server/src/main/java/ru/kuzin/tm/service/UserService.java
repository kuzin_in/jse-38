package ru.kuzin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.repository.IProjectRepository;
import ru.kuzin.tm.api.repository.ITaskRepository;
import ru.kuzin.tm.api.repository.IUserRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.api.service.IUserService;
import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.exception.entity.UserNotFoundException;
import ru.kuzin.tm.exception.field.*;
import ru.kuzin.tm.model.User;
import ru.kuzin.tm.util.HashUtil;

import java.sql.Connection;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ITaskRepository taskRepository;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final IPropertyService propertyService,
            @NotNull IConnectionService connectionService
    ) {
        super(userRepository, connectionService);
        this.propertyService = propertyService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void setRepositoryConnection(Connection connection) {
        repository.setRepositoryConnection(connection);
        this.projectRepository.setRepositoryConnection(connection);
        this.taskRepository.setRepositoryConnection(connection);
    }

    @Nullable
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user = new User();
        try {
            repository.setRepositoryConnection(connection);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            user = repository.add(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        @NotNull User user = new User();
        try {
            repository.setRepositoryConnection(connection);
            if (repository.isLoginExist(login)) throw new ExistsLoginException();
            if (password == null || password.isEmpty()) throw new PasswordEmptyException();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            if (email != null) {
                if (repository.isEmailExist(email)) throw new ExistsEmailException();
                user.setEmail(email);
            }
            user = repository.add(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        @NotNull User user = new User();
        try {
            repository.setRepositoryConnection(connection);
            if (repository.isLoginExist(login)) throw new ExistsLoginException();
            if (password == null || password.isEmpty()) throw new PasswordEmptyException();
            if (role == null) throw new RoleEmptyException();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            user = repository.add(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            user = repository.findOneById(id);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            user = repository.findByLogin(login);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            user = repository.findByEmail(email);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User remove(@Nullable final User model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            taskRepository.setRepositoryConnection(connection);
            projectRepository.setRepositoryConnection(connection);
            user = repository.remove(model);
            @NotNull final String userId = user.getId();
            taskRepository.clear(userId);
            projectRepository.clear(userId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;

    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            user = repository.findByLogin(login);
            user = repository.remove(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            taskRepository.setRepositoryConnection(connection);
            projectRepository.setRepositoryConnection(connection);
            user = repository.findByEmail(email);
            taskRepository.clear(user.getId());
            projectRepository.clear(user.getId());
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return this.remove(user);
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user = repository.update(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            user = repository.update(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull Connection connection = getConnection();
        boolean result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.isLoginExist(login);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        @NotNull Connection connection = getConnection();
        boolean result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.isEmailExist(email);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            repository.update(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable User user;
        try {
            repository.setRepositoryConnection(connection);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            repository.update(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}