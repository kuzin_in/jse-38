package ru.kuzin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.repository.ITaskRepository;
import ru.kuzin.tm.enumerated.DBColumns;
import ru.kuzin.tm.enumerated.DBTables;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.model.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    protected String getTableName() {
        return DBTables.TM_TASK.name();
    }

    @Override
    @NotNull
    public Task fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBColumns.ID_COLUMN.getColumnName()));
        task.setCreated(row.getTimestamp(DBColumns.CREATED_COLUMN.getColumnName()));
        task.setUserId(row.getString(DBColumns.USER_ID_COLUMN.getColumnName()));
        task.setName(row.getString(DBColumns.NAME_COLUMN.getColumnName()));
        task.setDescription(row.getString(DBColumns.DESCRIPTION_COLUMN.getColumnName()));
        task.setStatus(Status.toStatus(row.getString(DBColumns.STATUS_COLUMN.getColumnName())));
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull final Task model) throws Exception {
        @NotNull final String query = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?)",
                DBTables.TM_TASK.name(), DBColumns.ID_COLUMN.getColumnName(),
                DBColumns.CREATED_COLUMN.getColumnName(), DBColumns.USER_ID_COLUMN.getColumnName(),
                DBColumns.NAME_COLUMN.getColumnName(), DBColumns.DESCRIPTION_COLUMN.getColumnName(),
                DBColumns.STATUS_COLUMN.getColumnName(), DBColumns.PROJECT_ID_COLUMN.getColumnName());
        try (@NotNull PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getUserId());
            statement.setString(4, model.getName());
            statement.setString(5, model.getDescription());
            statement.setString(6, model.getStatus().getDisplayName());
            statement.setString(7, model.getProjectId());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?", getTableName(),
                DBColumns.USER_ID_COLUMN.getColumnName(), DBColumns.PROJECT_ID_COLUMN.getColumnName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public Task update(@NotNull Task model) throws Exception {
        @NotNull final String query = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBColumns.NAME_COLUMN.getColumnName(),
                DBColumns.DESCRIPTION_COLUMN.getColumnName(), DBColumns.STATUS_COLUMN.getColumnName(),
                DBColumns.PROJECT_ID_COLUMN.getColumnName(), DBColumns.ID_COLUMN.getColumnName());
        try (@NotNull PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getStatus().getDisplayName());
            statement.setString(4, model.getProjectId());
            statement.setString(5, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

}