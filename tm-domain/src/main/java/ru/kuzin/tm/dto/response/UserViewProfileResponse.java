package ru.kuzin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserViewProfileResponse extends AbstractUserResponse {

    public UserViewProfileResponse(@Nullable final User user) {
        super(user);
    }

}
